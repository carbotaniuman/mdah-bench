#!/usr/bin/env bash

if ! command -v redis-cli >/dev/null; then
  apt-get install -qq -y redis
fi

if ! redis-cli info /dev/null 2>&1; then
  echo "Starting redis..."
  redis-server >redis.log 2>&1 &
fi

export CWD="$(pwd)"
envsubst <settings.yaml.tmpl >config.yml

mkdir -pv cache

java \
  --enable-preview \
  -jar kmdah.jar >run.log 2>&1 &

export CLIENT_PID
CLIENT_PID=$(pgrep -f "kmdah")

echo "Client started at pid $CLIENT_PID"

#!/usr/bin/env bash

envsubst <settings.json.tmpl >settings.json

./mdathome-golang >run.log 2>&1 &

export CLIENT_PID
CLIENT_PID=$(pgrep -f "mdathome")

echo "Client started at pid $CLIENT_PID"

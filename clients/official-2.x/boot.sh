#!/usr/bin/env bash

envsubst <settings.sample.yaml >settings.yaml

java \
  -Dfile-level=off \
  -Dstdout-level=info \
  -jar mangadex_at_home.jar >run.log 2>&1 &

export CLIENT_PID
CLIENT_PID=$(pgrep -f "mangadex_at_home")

echo "Client started at pid $CLIENT_PID"

#!/usr/bin/env bash

set -uo pipefail

# Utils
#####################################################################

function eko() {
  prefix="$1"
  shift 1

  # we do want splitting mister shellcheck
  # shellcheck disable=SC2068
  $@ | while read -r line; do
    echo "$(date +%T.%N) - $prefix - $line"
  done
}

function eko_orchestrator() {
  # yes we do want splitting
  # shellcheck disable=SC2068
  eko "***" $@
}

# Per client run
#####################################################################

function kill_client() {
  (
    set -x
    kill -9 "$CLIENT_PID"
  ) 2>&1
}

function bench_client() {
  CLIENT_NAME=$1

  if [ -z "$CLIENT_NAME" ]; then
    eko_orchestrator "CLIENT_NAME is not defined!"
  fi

  echo "------------------------------------------------------"
  echo "Starting run for $CLIENT_NAME                         "
  echo "------------------------------------------------------"

  if ! pushd "clients/$CLIENT_NAME"; then
    echo "Directory clients/$CLIENT_NAME not found!"
    exit 1
  fi

  if ! source client.env; then
    echo "Cannot source client env config!"
    exit 1
  else
    echo "Using scheme: $SCHEME"
  fi

  export SCHEME="$SCHEME"

  source ./boot.sh 2>&1
  if ! popd; then
    echo "Can't return to root directory! Aborting!"
    kill_client
    exit 1
  fi

  # 10x5s attempts
  if ! ./scripts/healthcheck.sh 10 5 2>&1; then
    echo "Client never became healthy! Aborting!"
    kill_client
    exit 1
  fi

  if ! curl -k -s -o /dev/null -XGET "$SCHEME://localhost:44300/data/8172a46adc798f4f4ace6663322a383e/B18.png" 2>&1; then
    echo "Cannot preload B18.png!"
    kill_client
    exit 1
  fi
  echo "Preloaded B18.png..."

  if ! ./scripts/bench.sh 5000 2>&1; then
    echo "Benchmark loop failed!"
    kill_client
    exit 1
  fi
  echo "Finished benchmark!"

  kill_client
  echo "Finished run and shut down client"

}

# Global
#####################################################################

function run_all() {
  CLIENTS=()
  for client in clients/*; do
    eko "INIT" echo "Found client directory: $client"
    client="$(echo "$client" | cut -d '/' -f2)"
    CLIENTS+=("$client")
  done

  eko "INIT" echo "Will bench these clients: ${CLIENTS[*]}"

  unset client

  for CLIENT_NAME in "${CLIENTS[@]}"; do
    export CLIENT_NAME="$CLIENT_NAME"
    run_one "$CLIENT_NAME"
  done
}

function run_one() {
  if [ -f "secret.env" ]; then
    export SECRET
    SECRET=$(cat secret.env)
  fi

  if [ -z "${SECRET:-}" ]; then
    echo "SECRET is not defined. Create a file secret.env containing it."
  fi

  echo ""
  eko_orchestrator echo "Start run for: $CLIENT_NAME"
  if ! eko "$CLIENT_NAME" bench_client "$CLIENT_NAME"; then
    eko_orchestrator echo "Client $CLIENT_NAME did not successfully run! Aborting!"
    exit 1
  fi
}

if [ -n "${1:-}" ]; then
  export CLIENT_NAME="$1"
  run_one "$CLIENT_NAME"
else
  run_all
fi

# MDAH clients (unscientific) benchmarks

## Results

Clients tested:

- [Official](https://gitlab.com/mangadex-pub/mangadex_at_home)  2.0.0-rc13
- [Mdathome Golang](https://github.com/lflare/mdathome-golang)  1.9.1
- [kmdah](https://github.com/Tristan971/kmdah)                  0.3.34

| Client        | Rqps @ 10 conn    | Rqps @ 100 conn   | Rqps @ 200 conn   |
|:-----------   |:-----------:      |:-----------:      |:-----------:      |
| official-2.x | 1888.75 (avg 5.294 ms) | 1811.42 (avg 55.205 ms) | 1798.15 (avg 111.226 ms) |
| mdgo-1.x | 1773.32 (avg 5.639 ms) | 1690.92 (avg 59.139 ms) | 1606.34 (avg 124.507 ms) |
| kmdah-0.3.x | 5612.61 (avg 1.782 ms) | 5064.89 (avg 19.744 ms) | 5252.29 (avg 38.079 ms) |

## Some notes

It's in gitlab-ci, without any control to make sure every client gets exactly the same CPU/Memory/FS speeds. This is partially why it's not a scientific
comparison.

*kmdah* is not SSL-aware, which makes things easier for it. As it is meant to be used inside a kubernetes cluster, where a reverse proxy is always present. It
then just configures that reverse proxy at runtime with the certificate it receives from the backend.

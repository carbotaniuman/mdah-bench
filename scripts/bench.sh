#!/usr/bin/env bash

set -euo pipefail

echo "-----------------------------------------"
echo "Prepare run..."

mkdir -pv "bench/$CLIENT_NAME"

echo "-----------------------------------------"
echo "Warming..."
ab -n "$1" -c 100 "$SCHEME://127.0.0.1:44300/data/8172a46adc798f4f4ace6663322a383e/B18.png" | tee "bench/$CLIENT_NAME/bench_warm.log"

echo "-----------------------------------------"
echo "Benching... 10 connections"
ab -n 3000 -c 10 "$SCHEME://127.0.0.1:44300/data/8172a46adc798f4f4ace6663322a383e/B18.png" | tee "bench/$CLIENT_NAME/bench_10.log"

echo "-----------------------------------------"
echo "Benching... 100 connections"
ab -n 3000 -c 100 "$SCHEME://127.0.0.1:44300/data/8172a46adc798f4f4ace6663322a383e/B18.png" | tee "bench/$CLIENT_NAME/bench_100.log"

echo "-----------------------------------------"
echo "Benching... 200 connections"
ab -n 3000 -c 200 "$SCHEME://127.0.0.1:44300/data/8172a46adc798f4f4ace6663322a383e/B18.png" | tee "bench/$CLIENT_NAME/bench_200.log"

#!/usr/bin/env bash

set -euo pipefail

function rqps_cli_n() {
  cli=$1
  n=$2
  # Requests per second:    154.16 [#/sec] (mean)
  grep "Requests per second" "bench/$cli/bench_$n.log" | tr -s ' ' | cut -d' ' -f4
}

function mean_request_time() {
  cli=$1
  n=$2
  # Time per request:       125.292 [ms] (mean)
  grep "Time per request" "bench/$cli/bench_$n.log" | grep "(mean)" | tr -s ' ' | tr -d '[' | tr -d ']' | cut -d' ' -f4,5
}

CLIENT_SUMMARY_LINE=()

# shellcheck disable=SC2068
for CLIENT_PATH in bench/*; do
  CLIENT_NAME=$(echo "$CLIENT_PATH" | cut -d'/' -f2)
  echo "Generate summary for: $CLIENT_NAME"
  rqps_10=$(rqps_cli_n "$CLIENT_NAME" 10)
  mrq_10=$(mean_request_time "$CLIENT_NAME" 10)
  rqps_100=$(rqps_cli_n "$CLIENT_NAME" 100)
  mrq_100=$(mean_request_time "$CLIENT_NAME" 100)
  rqps_200=$(rqps_cli_n "$CLIENT_NAME" 200)
  mrq_200=$(mean_request_time "$CLIENT_NAME" 200)
  summary_line="| $CLIENT_NAME | $rqps_10 (avg ${mrq_10}) | $rqps_100 (avg ${mrq_100}) | $rqps_200 (avg ${mrq_200}) |"
  echo "Summary: $summary_line"
  CLIENT_SUMMARY_LINE+=("$summary_line")
done

cp -v result.template.md result.md.tmp

echo "Summaries:"
for summary in "${CLIENT_SUMMARY_LINE[@]}"; do
  echo "$summary"
  sed -i "/CLIENT_SUMMARY_LINE/a $summary" result.md.tmp
done

if ! [ -f "result.md.tmp" ]; then
  echo "Failed sed"
  exit 1
fi

grep -v "CLIENT_SUMMARY_LINE" result.md.tmp >"result-final.md.tmp"
rm -v result.md.tmp

multimarkdown -f "result-final.md.tmp" >"$1"
mv result-final.md.tmp README.md

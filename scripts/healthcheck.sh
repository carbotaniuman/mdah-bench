#!/usr/bin/env bash

MAX_ATTEMPTS=$1
SLEEP_SECS=$2

HEALTHCHECK_URL="$SCHEME://localhost:44300${HEALTH_ENDPOINT:/data/8172a46adc798f4f4ace6663322a383e/B18.png}"
echo "Using healthcheck url: $HEALTHCHECK_URL"

ATTEMPTS=0
while ! curl -k --no-progress-meter -o /dev/null -I -XGET "$HEALTHCHECK_URL"; do
  CUR_ATTEMPT=$((ATTEMPTS + 1))
  echo "Failed attempt $CUR_ATTEMPT/$MAX_ATTEMPTS - Sleeping $SLEEP_SECS before retry..."
  sleep "$SLEEP_SECS"

  ATTEMPTS=$CUR_ATTEMPT
  if [ "$ATTEMPTS" == "$MAX_ATTEMPTS" ]; then
    echo "Failed attempts. Aborting."
    exit 1
  fi
done

echo "Client is now healthy!"
